FROM php:7.4-apache

# install libraries and 3rd party tools
RUN apt update
RUN apt install -y --no-install-recommends \
    libpq-dev \
    libmcrypt-dev \
    libpng-dev \
    libfreetype6-dev \
    libxml2-dev \
    libjpeg62-turbo-dev \
    zlib1g-dev \
    graphicsmagick \
    libzip-dev
RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/
RUN docker-php-ext-install -j$(nproc) mysqli soap gd zip opcache intl pgsql pdo_pgsql
RUN a2enmod rewrite ssl

# set typo3 php options
RUN touch /usr/local/etc/php/conf.d/typo3.ini
RUN echo max_execution_time=240 >> /usr/local/etc/php/conf.d/typo3.ini
RUN echo max_input_vars=1500 >> /usr/local/etc/php/conf.d/typo3.ini

# install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/bin/composer

# add startup script
RUN mkdir /entrypoint
COPY startup.sh /entrypoint/.

# CLEANUP
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /usr/src/*

ENV FIRST_INSTALL=false
ENV INITIALIZE_EMPTY=false

WORKDIR /var/www/html

CMD ["bash", "/entrypoint/startup.sh"]